import java.util.ArrayList;
import java.util.List;

public class Facebook {
    Posting newPosting = new Posting();

    List<Posting> feed = new ArrayList<Posting>();

    public Posting getNewPosting() {
        return newPosting;
    }

    public void setNewPosting(Posting newPosting) {
        this.newPosting = newPosting;
    }

    public List<Posting> getFeed() {
        return feed;
    }

    public void setFeed(List<Posting> feed) {
        this.feed = feed;
    }
}
