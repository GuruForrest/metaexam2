import org.metaworks.annotation.AutowiredFromClient;
import org.metaworks.annotation.ServiceMethod;

public class Posting {
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @ServiceMethod(callByContent = true)
    public Facebook post(@AutowiredFromClient Facebook facebook) {
        facebook.getFeed().add(this);
        facebook.setNewPosting(new Posting());
        return facebook;
    }

}
